#!/bin/bash
./update.sh

#Basic utilities (desktops and servers)
sudo apt install -y vim bash-completion net-tools wget curl lsof htop locate zip unzip p7zip p7zip-full unattended-upgrades

#Remove snapd
sudo systemctl stop snapd
sudo apt remove --purge --assume-yes snapd gnome-software-plugin-snap
sudo rm -rf ~/snap/ /var/cache/snapd/ 

#Desktops utilities and apps
sudo apt install -y baobab bleachbit shotwell gnome-tweaks pavucontrol vlc syncthing numix-icon-theme-circle gnome-shell-extension-manager
sudo apt remove -y libreoffice*

#Install flatpak
sudo apt install flatpak gnome-software-plugin-flatpak gnome-software
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
#Desktop flatpak apps
flatpak install -y com.github.tchx84.Flatseal com.github.Eloston.UngoogledChromium org.onlyoffice.desktopeditors com.visualstudio.code org.signal.Signal com.spotify.Client com.discordapp.Discord 

#Other desktop apps
#sudo apt install -y virtualbox steam barrier nextcloud-desktop
#flatpak install -y com.github.iwalton3.jellyfin-mpv-shim
